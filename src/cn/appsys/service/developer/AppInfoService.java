package cn.appsys.service.developer;

import java.util.List;
import cn.appsys.pojo.AppInfo;

public interface AppInfoService {
	
	
	public List<AppInfo> getAppInfoList(String querySoftwareName,Integer queryStatus,
								Integer queryCategoryLevel1,Integer queryCategoryLevel2,
								Integer queryCategoryLevel3,Integer queryFlatformId,
								Integer devId,Integer currentPageNo,Integer pageSize)throws Exception;
	
		public int getAppInfoCount(String querySoftwareName,Integer queryStatus,
							Integer queryCategoryLevel1,Integer queryCategoryLevel2,
							Integer queryCategoryLevel3,Integer queryFlatformId,Integer devId)throws Exception;
	
		public AppInfo getAppInfo(Integer id,String APKName)throws Exception;
		
		public boolean add(AppInfo appInfo) throws Exception;

		public boolean modify(AppInfo appInfo)throws Exception;
		
		
		
}
